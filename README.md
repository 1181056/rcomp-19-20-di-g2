RCOMP 2019-2020 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1181056 - {Rui Afonso} 
  * 1181053 - {João Magalhães} 
  * 1180871 - {Maria Duarte} 
  * 1180017 - {Gabriel Pelosi} 
  * 1170894 - {Francisco Calejo}

Any team membership changes should be reported here, examples:

Member 8888888 ({First and last name}) has left the team on 2020-03-20

Member 7777777 ({First and last name}) has entered the team on 2020-04-5
# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

